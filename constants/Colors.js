const tintColor = '#2f95dc';

export default {
    primary: '#8E98FD',
    success: '#4DC591',
    danger: '#FE7747',
    warning: '#FCBA7E',
    info: '#182B88',
    text: '#333',
    mutedText: '#777',
    border: '#ddd',
    body: '#fff',
    input: '#fafafa',
    defaultButton: '#fafafa',
    focusBorder: '#d7d7d7',






    tintColor,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
};
