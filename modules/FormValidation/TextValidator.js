/* eslint-disable */
import React from 'react';
import {TextInput, View, Text} from 'react-native';
/* eslint-enable */
import Input from './Input';

export default class TextValidator extends Input {

    render() {
        /* eslint-disable no-unused-vars */
        const {
            errorMessage,
            validators,
            requiredError,
            underlineColorAndroid,
            errorStyle,
            style,
            validatorListener,
            ...rest
        } = this.props;
        /* eslint-enable */
        const error = this.state.isValid ? '' : this.getErrorMessage();
        return (
            <View position="relative">
                <TextInput
                    {...rest}
                    style={[style, !this.state.isValid ? errorStyle.textInput : null]}
                    ref={(r) => {
                        this.input = r;
                    }}
                />
                <View {...errorStyle.container}>
                    <Text style={errorStyle.text}>{error}</Text>
                </View>
            </View>
        );
    }
}
