# Black Forest Hackathon -> Smart City

### City Project Manager

Problem:

In Städten und Gemeinden werden oft Projekte realisiert, die gelegentlich von Bürgern als nicht Sinnvoll erachtet werden. Zudem haben Bürger keine Transparenz was Projekte in der Gemeinde/Stadt betrifft und nur selten die Möglichkeit haben direktes Feedback zu realisierten Projekten zu geben.

Lösung:

Den Bürgern wird eine Plattform gestellt um Projekte in auf Städte/Gemeinden Ebene zu realisieren und verwalten. Städte und Gemeinden können ihre anstehenden Projekte dort hinterlegen, Bürger können anschließend Anregungen und Feedback dazu geben oder allgemein über die Umsetzung des Projekts abstimmen.

### Techstack

- Frameworks:  React-Native, Expo.io
- Programmiersprache: Javascript

#### Expo.io

develop, build, deploy, and quickly iterate on iOS, Android, and web apps from the same JavaScript/TypeScript codebase

#### React-Native

React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.
Written in JavaScript—rendered with native code.
