import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
import TabBarIcon from '../components/tabbar/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import CommuneScreen from '../screens/CommuneScreen';
import ProjectsScreen from '../screens/ProjectsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import Colors from "../constants/Colors";
import CategoryScreen from "../screens/CategoryScreen";
import JoinScreen from "../screens/JoinScreen";

export default function () {

    const config = Platform.select({
        headerMode: 'none',
        default: {},
    });

    const HomeStack = createStackNavigator(
        {
            Home: HomeScreen,
        },
        config
    );

    HomeStack.navigationOptions = {
        tabBarLabel: 'Startseite',
        tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused}
                        color={Colors.primary}
                        name='home'/>
        ),
        tabBarOptions: {
            activeTintColor: Colors.primary
        }
    };

    HomeStack.path = '';

    const ProjectsStack = createStackNavigator(
        {
            Projects: ProjectsScreen,
            Category: CategoryScreen,
            Join: JoinScreen,
        },
        {
            headerMode: 'none',
            mode: 'modal',
        }
    );

    ProjectsStack.navigationOptions = {
        tabBarLabel: 'Projekte',
        tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused}
                        color={Colors.success}
                        name='widgets'/>
        ),
        tabBarOptions: {
            activeTintColor: Colors.success
        }
    };

    ProjectsStack.path = '';

    const CommuneStack = createStackNavigator(
        {
            Commune: CommuneScreen,
        },
        config
    );

    CommuneStack.navigationOptions = {
        tabBarLabel: 'Gemeinde',
        tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused}
                        color={Colors.warning}
                        name={'location-city'}/>
        ),
        tabBarOptions: {
            activeTintColor: Colors.warning
        }
    };

    CommuneStack.path = '';

    const ProfileStack = createStackNavigator(
        {
            Profile: ProfileScreen,
        },
        config
    );

    ProfileStack.navigationOptions = {
        tabBarLabel: 'Profil',
        tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused}
                        color={Colors.danger}
                        name={'account-circle'}/>
        ),
        tabBarOptions: {
            activeTintColor: Colors.danger
        }
    };

    ProfileStack.path = '';


    return createBottomTabNavigator(
        {
            HomeStack,
            ProjectsStack,
            CommuneStack,
            ProfileStack,
        }
    );
}
