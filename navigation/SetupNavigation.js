import {createStackNavigator} from "react-navigation";
import WelcomeScreen from "../screens/WelcomeScreen";
import LocationScreen from "../screens/LocationScreen";
import SalutationScreen from "../screens/SalutationScreen";
import SigninScreen from "../screens/SigninScreen";
import VertifyScreen from "../screens/VertifyScreen";

export default function () {
    return createStackNavigator(
        {
            Welcome: WelcomeScreen,
            Location: LocationScreen,
            Salutation: SalutationScreen,
            Signin: SigninScreen,
            Vertify: VertifyScreen,
        },
        {
            headerMode: 'none',
        }
    );
};