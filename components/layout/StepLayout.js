import React, {Component} from 'react';
import {StyleSheet, View, SafeAreaView, Dimensions} from 'react-native';
import Colors from "../../constants/Colors";
import IconButton from "../button/IconButton";
import ColorBlockButton from "../button/ColorBlockButton";
import DefaultBlockButton from "../button/DefaultBlockButton";
import ParallaxNavbar from '../../modules/Navbar/ParallaxNavbar';


export default class StepLayout extends Component {


    renderNavbar = () => {
        const {onPressBack, backIcon} = this.props;
        return (
            <View style={[styles.header]}>
                {onPressBack && <IconButton icon={backIcon || 'keyboard-arrow-left'}
                                            onPress={onPressBack}
                                            containerStyle={styles.backButton}/>}
            </View>
        );
    };


    renderContent = () => {
        const {children, bodyContainer} = this.props;
        return (
            <View style={[styles.body, bodyContainer]}>
                {children}
            </View>
        );
    };

    render() {
        const {color, onPressContinue, continueText, title, defaultContinueButton} = this.props;
        return (
            <SafeAreaView style={[styles.scrollView, color && {backgroundColor: color}]}>
                <ParallaxNavbar headerMinHeight={88}
                                headerMaxHeight={130}
                                extraScrollHeight={20}
                                backgroundColor={color || Colors.primary}
                                navbarColor={color || Colors.primary}
                                title={title}
                                titleStyle={styles.title}
                                renderNavBar={this.renderNavbar}
                                renderContent={this.renderContent}
                                containerStyle={styles.container}
                                contentContainerStyle={styles.contentContainer}
                                innerContainerStyle={styles.container}/>
                {onPressContinue && <View style={styles.continueButtonContainer}>
                    {defaultContinueButton ? (
                            <DefaultBlockButton title={continueText || 'Nächster Schritt'}
                                                onPress={onPressContinue}/>)
                        : (
                            <ColorBlockButton title={continueText || 'Nächster Schritt'}
                                              color={color}
                                              onPress={onPressContinue}/>
                        )}
                </View>}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        flexGrow: 1,
    },
    scrollView: {
        backgroundColor: Colors.primary,
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'transparent',
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#fff'
    },
    continueButtonContainer: {
        height: 76,
        borderTopColor: Colors.border,
        borderTopWidth: 1,
        padding: 16,
        backgroundColor: Colors.body
    },
    backButton: {
        position: 'absolute',
        top: 16,
        left: 16,
    },
    body: {
        backgroundColor: Colors.body,
        marginTop: -16,
        flex: 1,
        height: '100%',
        minHeight: Dimensions.get('window').height - 130,
        borderTopLeftRadius: 32,
        borderTopRightRadius: 32,
        paddingHorizontal: 16,
        paddingTop: 32,
        paddingBottom: 16
    },
});
