import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView} from 'react-native';
import {primary, text} from '../../constants/Colors';
import {first} from 'lodash';
import ImageSlider from 'react-native-image-slider';
import Timeline from 'react-native-timeline-feed';
import IconButton from "../button/IconButton";
import ColorBlockButton from "../button/ColorBlockButton";
import Colors from "../../constants/Colors";
import ListItem from "../list/ListItem";
import {Rating} from 'react-native-ratings';
import TabbarContainer from "../tabbar/TabbarContainer";

export default class MapSwiperItem extends Component {

    onPressCard = () => {
        const {open, onPressOpen} = this.props;
        if (!open) {
            onPressOpen();
        }
    };

    onPressJoin = () => {
        const {navigation} = this.props;
        navigation.navigate('Join');
    };

    render() {
        const {data, open, onPressClose} = this.props;
        return open ? (
            <View style={styles.view}>
                <ScrollView style={styles.scrollView}>
                    <View style={open ? styles.openImageContainer : styles.imageContainer}>
                        <ImageSlider images={data.images || []}
                                     style={open ? styles.openImageSlider : styles.imageSlider}/>
                        <IconButton icon='close' containerStyle={styles.closeButton} onPress={onPressClose}/>
                    </View>
                    <View style={styles.body}>
                        <Text style={styles.openName}>{data.name}</Text>
                        <View style={styles.section}>
                            <Text style={styles.headliner}>Kurzbeschreibung</Text>
                            <Text style={styles.descriptionText}>
                                {data.description}
                            </Text>
                        </View>
                        {data.category === 1 && <View style={styles.section}>
                            <Text style={styles.headliner}>Pro / Contra</Text>
                            <TabbarContainer/>
                        </View>}
                        <View style={styles.section}>
                            <Text style={styles.headliner}>Bewertung</Text>
                            <View style={{flexDirection: 'row'}}>
                                <Rating ratingCount={5} startingValue={5} imageSize={22}/>
                                <View style={{flex: 1, paddingLeft: 8}}>
                                    <Text>Finanzierbarkeit</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Rating ratingCount={5} startingValue={5} imageSize={22}/>
                                <View style={{flex: 1, paddingLeft: 8}}>
                                    <Text>Umsetzbarkeit</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Rating ratingCount={5} startingValue={4} imageSize={22}/>
                                <View style={{flex: 1, paddingLeft: 8}}>
                                    <Text>Lebensqualität</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Rating ratingCount={5} startingValue={3} imageSize={22}/>
                                <View style={{flex: 1, paddingLeft: 8}}>
                                    <Text>Bedarf / Nutzen / Mehrwert</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Rating ratingCount={5} startingValue={2} imageSize={22}/>
                                <View style={{flex: 1, paddingLeft: 8}}>
                                    <Text>Einfluss auf Stadtbild</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <Text style={styles.headliner}>Status</Text>
                            <Timeline showTime={false}
                                      innerCircle={'dot'}
                                      circleSize={20}
                                      lineColor={Colors.border}
                                      circleColor={Colors.border}
                                      data={[
                                          {
                                              title: 'Start',
                                              description: '',
                                              circleColor: Colors.success,
                                              lineColor: Colors.success
                                          },
                                          {
                                              title: 'Bewertung / Feedback',
                                              description: '',
                                              circleColor: Colors.success,
                                              lineColor: data.category === 1 ? Colors.success : void 0
                                          },
                                          {
                                              title: 'Plannung / Projektarbeit',
                                              description: '',
                                              circleColor: data.category === 1 ? Colors.success : void 0
                                          },
                                          {
                                              title: 'Entscheid von Gemeinderat',
                                              description: ''
                                          },
                                          {title: 'Umsetzung', description: ''},
                                          {title: 'Fertig', description: ''},
                                      ]}/>
                        </View>
                        {data.category === 1 && <View style={styles.section}>
                            <Text style={styles.headliner}>Einschränkung</Text>
                            <ListItem icon={'warning'} iconColor={Colors.warning}
                                      title={'Kurze Montagezeit'}/>
                        </View>}
                        {data.category === 1 && <View style={styles.section}>
                            <Text style={styles.headliner}>Quellen</Text>
                            <ListItem icon={'open-in-new'} iconColor={Colors.success}
                                      title={'www.wikipedia.com/dsafdsaf/3434'}/>
                            <ListItem icon={'open-in-new'} iconColor={Colors.success}
                                      title={'www.projekt-arbeit.com/3/3434'}/>
                        </View>}
                        <View style={styles.section}>
                        </View>
                        <View style={styles.section}>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.continueButtonContainer}>
                    <ColorBlockButton title={data.category === 1 ? 'Jetzt mitmachen' : 'Jetzt bewerten'}
                                      color={Colors.success}
                                      onPress={this.onPressJoin}/>
                </View>
            </View>
        ) : <TouchableOpacity style={styles.card}
                              activeOpacity={1}
                              autoHardwareBack={false}
                              onPress={() => this.onPressCard()}>
            <View style={open ? styles.openImageContainer : styles.imageContainer}>
                <ImageSlider images={[first(data.images)]}
                             style={styles.imageSlider}/>
                <View style={styles.blocker}/>
            </View>
            <View style={styles.body}>
                <Text style={styles.name}>{data.name}</Text>
            </View>
        </TouchableOpacity>;
    }
}

const styles = StyleSheet.create({
    scrollView: {
        flex: 1
    },
    view: {

        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
    },
    card: {
        backgroundColor: '#fff',
        borderTopRightRadius: 16,
        borderTopLeftRadius: 16,
        height: Dimensions.get('window').height
    },
    imageContainer: {
        height: 190
    },
    openImageContainer: {
        height: 320
    },
    imageSlider: {
        borderTopRightRadius: 16,
        borderTopLeftRadius: 16,
    },
    openImageSlider: {
        borderBottomRightRadius: 16,
        borderBottomLeftRadius: 16,
    },
    closeButton: {
        position: 'absolute',
        top: 32,
        left: 16,
    },
    moreButton: {
        position: 'absolute',
        top: 32,
        left: 16,
    },
    body: {
        padding: 16,
    },
    section: {
        paddingVertical: 16,
    },
    blocker: {
        position: 'absolute',
        bottom: 0,
        top: 0,
        right: 0,
        left: 0
    },
    name: {
        fontSize: 18,
        fontWeight: "bold"
    },
    descriptionText: {
        color: Colors.mutedText
    },
    openName: {
        fontSize: 28,
        fontWeight: "bold"
    },
    headliner: {
        fontSize: 18,
        fontWeight: "bold",
        paddingBottom: 8
    },
    continueButtonContainer: {
        position: 'absolute',
        bottom: 82,
        left: 0,
        right: 0,
        zIndex: 1,
        height: 76,
        borderTopColor: Colors.border,
        borderTopWidth: 1,
        padding: 16,
        backgroundColor: Colors.body
    },
});
