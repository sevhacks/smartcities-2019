import React, {Component} from 'react';
import {View, Dimensions, StyleSheet, Animated} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import MapSwiperItem from "./MapSwiperItem";
import {isNumber} from 'lodash';
import Loader from "../loader/Loader";
import {primary, text} from '../../constants/Colors';


export default class MapSwiper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeIndex: void 0
        };
        this.moveAnimation = new Animated.ValueXY({x: 0, y: Dimensions.get('window').height - 330})
    }

    onPressOpenItem = (index) => {
        this.setState({activeIndex: index});
        setTimeout(() => {
            Animated.spring(this.moveAnimation, {
                toValue: {x: 0, y: 0},
                speed: 6
            }).start();
        });
    };

    onPressCloseItem = () => {
        Animated.spring(this.moveAnimation, {
            toValue: {x: 0, y: Dimensions.get('window').height - 330},
            speed: 6
        }).start();
        setTimeout(() => {
            this.setState({activeIndex: void 0});
        }, 100);
    };

    render() {
        const {data, loading, onChangeCarousel, navigation} = this.props;
        const {activeIndex} = this.state;
        const open = isNumber(activeIndex);
        return (
            <Animated.View
                style={[styles.container, (open && {height: Dimensions.get('window').height}), this.moveAnimation.getLayout()]}>
                <Carousel ref={(element) => this.carousel = element}
                          data={[...(data || []), {}]}
                          onSnapToItem={onChangeCarousel}
                          scrollEnabled={!open}
                          renderItem={({item, index}) => item.name ? (
                              <MapSwiperItem data={item}
                                             navigation={navigation}
                                             open={activeIndex === index}
                                             onPressClose={this.onPressCloseItem}
                                             onPressOpen={() => this.onPressOpenItem(index)}/>
                          ) : (
                              <View style={styles.loader}><Loader/></View>
                          )}
                          layoutCardOffset={9}
                          sliderWidth={Dimensions.get('window').width}
                          itemWidth={open ? Dimensions.get('window').width : (Dimensions.get('window').width / 4) * 3}/>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 260,
        position: 'absolute',
        bottom: 0,
        zIndex: 1,
        width: '100%'
    },
    loader: {
        height: 500,
        backgroundColor: '#fff'
    },
});
