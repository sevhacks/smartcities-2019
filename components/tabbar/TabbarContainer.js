import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {primary, text} from '../../constants/Colors';
import Colors from "../../constants/Colors";
import ListItem from "../list/ListItem";


export default class TabbarContainer extends Component {
    state = {
        tab: 0
    };

    onPressTab = (tab) =>{
        this.setState({tab});
    };

    render() {
        const {tab} = this.state;
        return (
            <View>
                <View style={{flexDirection: 'row', width: '100%'}}>
                    <TouchableOpacity onPress={() => this.onPressTab(0)} style={[styles.tabbar]}>
                        <Text style={[styles.tabbarText, tab === 0 && {color: Colors.success}]}>Pro</Text>
                    </TouchableOpacity>
                    <View style={{width: 1, backgroundColor: Colors.border}}/>
                    <TouchableOpacity onPress={() => this.onPressTab(1)} style={[styles.tabbar]}>
                        <Text style={[styles.tabbarText, tab === 1 && {color: Colors.success}]}>Contra</Text>
                    </TouchableOpacity>
                </View>
                {tab === 0 && <View>
                    <ListItem icon={'add-circle'} iconColor={Colors.success}
                              title={'Lebensqualität'}/>
                    <ListItem icon={'add-circle'} iconColor={Colors.success}
                              title={'Familienfreundlich'}/>
                </View>}
                {tab === 1 && <View>
                    <ListItem icon={'remove-circle'} iconColor={Colors.danger}
                              title={'Wassernähe (Unfallgefahr)'}/>
                    <ListItem icon={'remove-circle'} iconColor={Colors.danger}
                              title={'Privatsphäre durch Gaffer'}/>
                </View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabbar: {
        flex: 1,
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabbarText: {
        fontWeight: "bold",
        fontSize: 16
    }
});
