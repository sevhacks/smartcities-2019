import React, {Component} from 'react';
import {Icon} from 'react-native-elements';
import {StyleSheet, View, Text, TouchableOpacity, Platform} from 'react-native';
import {TextValidator} from '../../modules/FormValidation';
import {isString, isBoolean} from "lodash";
import Colors from "../../constants/Colors";

export default class TextInputField extends Component {
    state = {
        isFocus: false,
        isPasswordEyeIsOn: false,
    };

    _onBlur = () => {
        this.setState({isFocus: false});
    };

    _onFocus = () => {
        this.setState({isFocus: true});
    };

    _onPressPasswordEye = () => {
        const {isPasswordEyeIsOn} = this.state;
        this.setState({isPasswordEyeIsOn: !isPasswordEyeIsOn});
    };

    _onChangeText = (value) => {
        const {name, onChangeText} = this.props;
        onChangeText({[name]: value});
    };

    render() {
        const {style, name, label, secureTextEntry, containerStyle, multiline, validators, errorMessages, autoCapitalize, autoCorrect, keyboardType, placeholder, value, editable = true, numberOfLines} = this.props;
        const {isFocus, isPasswordEyeIsOn} = this.state;
        const passwordIcon = (
            <TouchableOpacity style={[styles.passwordEyeButton, isString(label) && styles.marginTopFix]}
                              onPress={this._onPressPasswordEye}>
                {isPasswordEyeIsOn ?
                    <Icon size={25} name='remove-red-eye' color={Colors.textMute}/> :
                    <Icon size={25} name='visibility-off' color={Colors.textMute}/>}
            </TouchableOpacity>
        );
        return (
            <View style={[styles.container, containerStyle]}>
                {isString(label) ? <Text style={styles.label}>{label}</Text> : null}
                <TextValidator autoCapitalize={autoCapitalize}
                               autoCorrect={autoCorrect}
                               keyboardType={keyboardType}
                               validators={validators}
                               placeholder={placeholder}
                               errorMessages={errorMessages}
                               multiline={multiline}
                               value={value}
                               numberOfLines={numberOfLines}
                               editable={editable}
                               name={name}
                               onBlur={this._onBlur}
                               onFocus={this._onFocus}
                               onChangeText={this._onChangeText}
                               secureTextEntry={secureTextEntry && !isPasswordEyeIsOn}
                               style={[
                                   styles.input, secureTextEntry && styles.passwordInput,
                                   multiline && styles.multiInput,
                                   isFocus && styles.focus,
                                   editable ? {} : styles.editable,
                                   style
                               ]}/>
                {secureTextEntry ? passwordIcon : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingBottom: 8
    },
    input: {
        borderRadius: 21,
        backgroundColor: Colors.input,
        paddingVertical: Platform.OS === 'ios' ? 12 : 6,
        paddingHorizontal: 16,
        color: Colors.text,
        borderWidth: 1,
        borderColor: Colors.border,
        marginTop: 5,
    },
    focus: {
        backgroundColor: Colors.body,
        borderColor: Colors.focusBorder,
    },
    label: {
        fontWeight: 'bold'
    },
    multiInput: {
        paddingTop: 16,
        paddingBottom: 16
    },
    passwordInput: {
        paddingRight: 25 + (2 * 12)
    },
    passwordEyeButton: {
        position: 'absolute',
        right: 12,
        marginTop: 12
    },
    marginTopFix: {
        marginTop: 32
    },
    editable: {
        backgroundColor: Colors.border,
    }
});
