import React, {Component} from 'react';
import {StyleSheet, View, Platform, TextInput, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import {googleKey} from "../../constants/Keys";
import Colors from "../../constants/Colors";

export default class SearchField extends Component {
    state = {
        isFocus: false,
    };

    onBlur = () => {
        this.setState({isFocus: false});
    };

    onFocus = () => {
        this.setState({isFocus: true});
    };

    render() {
        const {isFocus} = this.state;
        const {value, onPressClear, onChange, placeholder, style} = this.props;
        const clearButton = (
            <TouchableOpacity style={styles.clearIcon}
                              onPress={onPressClear}>
                <Icon size={20}
                      name='close'
                      color={Colors.text}/>
            </TouchableOpacity>
        );

        return (
            <View>
                <View style={[styles.container, style]}>
                    <TextInput autoFocus
                               onBlur={this.onBlur}
                               onFocus={this.onFocus}
                               placeholder={placeholder}
                               style={[styles.input, isFocus && styles.focus]}
                               onChangeText={onChange}
                               value={value}/>
                    {value.length > 0 ? clearButton : null}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        width: '100%',
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 16,
        paddingTop: 32,
        backgroundColor: Colors.body,
        borderBottomColor: Colors.border,
        borderBottomWidth: 1,
    },
    input: {
        color: Colors.text,
        padding: Platform.OS === 'ios' ? 12 : 6,
        paddingRight: 40,
        paddingLeft: 16,
        backgroundColor: Colors.input,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 30
    },
    focus: {
        backgroundColor: Colors.body,
        borderColor: Colors.focusBorder,
    },
    clearIcon: {
        zIndex: 1,
        position: 'absolute',
        top: 16 + 13,
        right: 38,
    },
});
