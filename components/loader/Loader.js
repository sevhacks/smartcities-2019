import React, {Component} from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import Colors from "../../constants/Colors";

export default class Loader extends Component {
    render() {
        const {containerStyle, color, size} = this.props;
        return (
            <View style={[styles.container, containerStyle]}>
                <ActivityIndicator size={size || "small"} color={color || Colors.primary}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
