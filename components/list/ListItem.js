import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Avatar} from 'react-native';
import {ListItem as ListItemNative, Divider} from 'react-native-elements';
import Colors from "../../constants/Colors";

export default class ListItem extends Component {

    render() {
        const {title, icon, wrapperStyle, containerStyle, iconColor, onPress, divider, avatar, iconType, subtitle} = this.props;
        return (
            <View>
                <TouchableOpacity onPress={onPress}>
                    <ListItemNative wrapperStyle={[styles.wrapperStyle, wrapperStyle]}
                                    titleNumberOfLines={5}
                                    subtitleNumberOfLines={5}
                                    titleStyle={avatar ? styles.avatarTitle : styles.title}
                                    containerStyle={[avatar ? styles.avatarContainerStyle : styles.containerStyle, containerStyle]}
                                    rightIcon={{name: icon, color: 'transparent'}}
                                    leftIcon={{
                                        name: icon,
                                        color: iconColor || Colors.primary,
                                        type: iconType || 'material'
                                    }}
                                    avatar={avatar}
                                    avatarStyle={avatar && styles.avatar}
                                    avatarOverlayContainerStyle={avatar && styles.avatar}
                                    title={title}
                                    featuredSubtitleStyle={{color: Colors.mutedText}}
                                    subtitle={subtitle}/>
                </TouchableOpacity>
                {divider ? <Divider style={styles.listItemDivider}/> : null}
            </View>
        );
    }
}

/* STYLE */
const styles = StyleSheet.create({
    wrapperStyle: {
    },
    containerStyle: {
        paddingLeft: 16,
        paddingRight: 24,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    avatarContainerStyle: {
        paddingLeft: 0,
        paddingRight: 24,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    avatar: {
        borderRadius: 8,
        backgroundColor: 'transparent'
    },
    listItemDivider: {
        marginLeft: 24,
        marginRight: 16,
        backgroundColor: Colors.border
    },
    leftIcon: {
        display: 'none'
    },
    title: {
        color: Colors.text,
    },
    avatarTitle: {
        color: Colors.text,
        paddingLeft: 8
    },
});
