import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {border, text} from '../../constants/Colors';
import {Icon} from 'react-native-elements';

export default class IconButton extends Component {

    render() {
        const {icon, onPress, containerStyle} = this.props;
        return (
            <Icon raised
                  reverse
                  name={icon || 'close'}
                  size={22}
                  color='rgba(255, 255, 255, 0.7)'
                  iconStyle={{color: text}}
                  containerStyle={containerStyle}
                  onPress={onPress}/>
        );
    }
}

const styles = StyleSheet.create({
});
