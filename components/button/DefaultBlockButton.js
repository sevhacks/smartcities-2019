import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import Colors from "../../constants/Colors";
import {MaterialIcons} from '@expo/vector-icons';

export default class DefaultBlockButton extends Component {

    _disabledFunction = () => false;

    render() {
        const {title, buttonStyle, textStyle, onPress, disabled = false, icon} = this.props;
        return (
            <TouchableOpacity style={[styles.button, buttonStyle]}
                              onPress={disabled ? this._disabledFunction : onPress}>
                {title && <Text style={[styles.text, textStyle]}>{title}</Text>}
                {icon && <MaterialIcons name={icon}
                                        size={22}
                                        color={Colors.text}/>}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        width: '100%',
        padding: 12,
        backgroundColor: Colors.defaultButton,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: Colors.text,
    },
});
