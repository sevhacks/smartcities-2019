import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import Colors from "../../constants/Colors";

export default class ColorBlockButton extends Component {

    _disabledFunction = () => false;

    render() {
        const {title, buttonStyle, textStyle, onPress, disabled = false, color} = this.props;
        return (
            <TouchableOpacity style={[styles.button, buttonStyle, color && {
                backgroundColor: color,
                borderColor: color
            }]}
                              onPress={disabled ? this._disabledFunction : onPress}>
                <Text style={[styles.text, textStyle]}>{title}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        width: '100%',
        padding: 12,
        backgroundColor: Colors.primary,
        borderColor: Colors.primary,
        borderWidth: 1,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: Colors.body,
    },
});
