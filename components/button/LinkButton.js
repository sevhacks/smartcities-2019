import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import Colors from "../../constants/Colors";

export default class LinkButton extends Component {

    _disabledFunction = () => false;

    render() {
        const {title, buttonStyle, textStyle, onPress, disabled = false, color} = this.props;
        return (
            <TouchableOpacity style={[styles.button, buttonStyle]}
                              onPress={disabled ? this._disabledFunction : onPress}>
                <Text style={[styles.text, textStyle, color && {color: color}]}>{title}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        padding: 12,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: Colors.primary,
    },
});
