import {googleKey} from "../constants/Keys";

export default class GoogleHttpService {

    static getAddressById(id, icon) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve([])
            }, 1000)
        });
    }

    static getAddressesByName(name) {
        return fetch(`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${googleKey}&input=${name}&libraries=places&language=en`)
            .then(response => response.json())
            .then(({predictions}) => {
                return predictions.map(item => ({
                    _id: item.place_id,
                    placeId: item.place_id,
                    location: item.description,
                    icon: 'location-on'
                }))
            });
    }

    static getAddressByLatlng({latitude, longitude}) {
        return new Promise((resolve, reject) => {
            return fetch(`https://maps.googleapis.com/maps/api/geocode/json?key=${googleKey}&latlng=${latitude},${longitude}&libraries=places&language=de`)
                .then(response => response.json())
                .then(({results}) => {
                    let local = results.find(item => {
                        return item.types.some(type => ['locality'].indexOf(type) !== -1);
                    });
                    if (!local) {
                        local = results[0];
                    }
                    if (local) {
                        return resolve({
                            placeId: local.place_id,
                            location: local.formatted_address,
                            northeast: [local.geometry.viewport.northeast.lng, local.geometry.viewport.northeast.lat],
                            southwest: [local.geometry.viewport.southwest.lng, local.geometry.viewport.southwest.lat],
                            latitude: local.geometry.location.lat,
                            longitude: local.geometry.location.lng,
                            icon: 'location-on'
                        });
                    }
                    return results.length > 0 ? resolve({
                        placeId: results[0].place_id,
                        location: results[0].formatted_address,
                        northeast: [results[0].geometry.viewport.northeast.lng, results[0].geometry.viewport.northeast.lat],
                        southwest: [results[0].geometry.viewport.southwest.lng, results[0].geometry.viewport.southwest.lat],
                        latitude: results[0].geometry.location.lat,
                        longitude: results[0].geometry.location.lng,
                        icon: 'location-on'
                    }) : resolve({
                        latitude,
                        longitude,
                        icon: 'location-on'
                    });
                })
                .catch((error) => reject(error));
        });
    }
}
