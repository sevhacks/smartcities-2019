import {AsyncStorage} from 'react-native';
import {isString, isBoolean} from 'lodash';

export default class StoreService {

    static setFirstConnect() {
        return AsyncStorage.setItem('FirstConnect', JSON.stringify(true));
    }

    static getFirstConnect() {
        return new Promise((resolve) => {
            AsyncStorage.getItem('FirstConnect')
                .then((data) => {
                    const bool = JSON.parse(data);
                    resolve(isBoolean(bool) ? bool : void 0);
                })
                .catch(() => resolve());
        });
    }

}
