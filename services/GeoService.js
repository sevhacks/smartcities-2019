
export default class GeoService {
    static earthRadiusInMetre = 6371e3;

    static getRegionByCoordinates(points) {
        // points should be an array of { latitude: X, longitude: Y }
        if (points.length > 0) {
            let minX, maxX, minY, maxY;

            // init first point
            ((point) => {
                minX = point.latitude;
                maxX = point.latitude;
                minY = point.longitude;
                maxY = point.longitude;
            })(points[0]);

            // calculate rect
            points.map((point) => {
                minX = Math.min(minX, point.latitude);
                maxX = Math.max(maxX, point.latitude);
                minY = Math.min(minY, point.longitude);
                maxY = Math.max(maxY, point.longitude);
            });

            const midX = (minX + maxX) / 2;
            const midY = (minY + maxY) / 2;
            const deltaX = (maxX - minX);
            const deltaY = (maxY - minY);

            return {
                latitude: midX,
                longitude: midY,
                latitudeDelta: deltaX,
                longitudeDelta: deltaY
            };
        }
        return null;
    }

    static getRegionByLocation({latitude, longitude}, distance) {
        distance = distance / 2;
        const circumference = 40075;
        const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
        const angularDistance = distance / circumference;

        const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
        const longitudeDelta = Math.abs(Math.atan2(
            Math.sin(angularDistance) * Math.cos(latitude),
            Math.cos(angularDistance) - Math.sin(latitude) * Math.sin(latitude))
        );

        return {
            latitude: latitude,
            longitude: longitude,
            latitudeDelta,
            longitudeDelta,
        }
    }

    static boundsToRegion(bounds, aspectRatio = 1, margin = 1) {
        const [latitude, longitude] = GeoService.boundsToCenter(bounds);
        const R = (GeoService.calculateDistance(bounds[0], {latitude, longitude})) * margin;

        return {
            latitude,
            longitude,
            ...GeoService.calculateRegionDelta({latitude}, R, aspectRatio),
        };
    }

    static calculateRegionDelta({latitude}, radiusInMetre, aspectRatio = 1) {
        const radiusInRad = radiusInMetre / earthRadiusInMetre;
        const longitudeDelta = GeoService.rad2deg(radiusInRad / Math.cos(GeoService.deg2rad(latitude)));
        const latitudeDelta = aspectRatio * GeoService.rad2deg(radiusInRad);

        return {
            latitudeDelta,
            longitudeDelta,
        };
    }

    static boundsToCenter(bounds) {
        return bounds.reduce((coordinate1, coordinate2) => [
            average([coordinate1.latitude, coordinate2.latitude]),
            average([coordinate1.longitude, coordinate2.longitude]),
        ]);
    }

    static calculateDistance(coord1, coord2) {
        const R = earthRadiusInMetre;
        const lat1 = coord1.latitude;
        const lat2 = coord2.latitude;
        const lon1 = coord1.longitude;
        const lon2 = coord2.longitude;
        const phi1 = GeoService.deg2rad(lat1);
        const phi2 = GeoService.deg2rad(lat2);
        const deltaPhi = GeoService.deg2rad(lat2 - lat1);
        const deltaLambda = GeoService.deg2rad(lon2 - lon1);

        const a = Math.sin(deltaPhi / 2) * Math.sin(deltaPhi / 2) +
            Math.cos(phi1) * Math.cos(phi2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c
    }

    static deg2rad = n => n * Math.PI / 180;
    static rad2deg = n => n / Math.PI * 180;
    static average = numbers => numbers.reduce((sum, n) => sum + n, 0) / numbers.length;


    static getCoordinatesByBounds(bounds) {
        const array = [];
        array.push([bounds.northeast.lng, bounds.northeast.lat]);
        array.push([bounds.northeast.lng, bounds.northeast.lat]);
    }

    static getDistance([lat1, lon1], [lat2, lon2]) {
        if ((lat1 === lat2) && (lon1 === lon2)) {
            return 0;
        }
        else {
            const radlat1 = Math.PI * lat1 / 180;
            const radlat2 = Math.PI * lat2 / 180;
            const theta = lon1 - lon2;
            const radtheta = Math.PI * theta / 180;
            let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return dist;
        }
    }
}
