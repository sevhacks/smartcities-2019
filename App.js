import React, {useState} from 'react';
import {StatusBar, StyleSheet, View, Platform} from 'react-native';
import {AppLoading} from 'expo';
import * as Font from 'expo-font';
import {Util} from 'expo';
import {Ionicons} from '@expo/vector-icons';
import StoreService from "./services/StoreService";
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import MainTabNavigator from './navigation/MainTabNavigator';
import SetupNavigation from "./navigation/SetupNavigation";

export default function App(props) {
    const [isLoadingComplete, setLoadingComplete, firstConnect] = useState(false);


    if (!isLoadingComplete && !props.skipLoadingScreen) {
        return (
            <AppLoading
                startAsync={loadResourcesAsync}
                onError={handleLoadingError}
                onFinish={() => handleFinishLoading(setLoadingComplete)}
            />
        );
    } else {
        const AppNavigation = createAppContainer(
            createSwitchNavigator({
                // You could add another route here for authentication.
                // Read more at https://reactnavigation.org/docs/en/auth-flow.html
                Main: MainTabNavigator(),
                Setup: SetupNavigation(),
            },{
                initialRouteName: !firstConnect ? 'Setup' : 'Main',
            })
        );
        return (
            <View style={styles.container}>
                {Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
                <AppNavigation/>
            </View>
        );
    }
}

async function loadResourcesAsync() {

    const firstConnect = await StoreService.getFirstConnect();
    if (!firstConnect) {
        this.setState({firstConnect: true});
    }


    await Promise.all([
        Font.loadAsync({
            // This is the font that we are using for our tab bar
            ...Ionicons.font,
            // We include SpaceMono because we use it in HomeScreen.js. Feel free to
            // remove this if you are not using it in your app
            'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        }),
    ]);
}

function handleLoadingError(error) {
    // In this case, you might want to report the error to your error reporting
    // service, for example Sentry
    console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
    setLoadingComplete(true);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
});