import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors from "../constants/Colors";

export default class WelcomeScreen extends Component {
    state = {
        email: ''
    };

    onPressContinue = async () => {
        const { navigation } = this.props;
        navigation.navigate('Location', {});

    };

    onPressBack = () => {

    };

    onChangeText = (value) => {
        this.setState({ ...value });
    };

    render() {
        const { } = this.props;
        const { email } = this.state;
        return (
            <StepLayout title={'Willkommen zu Civibus'}
                color={Colors.danger}
                onPressContinue={this.onPressContinue}
                continueText={'jetzt loslegen'}>
                <View style={styles.container}>
                <View>
                        <Image source={require('../assets/images/WortBild_CIVIBUS.png')} style={styles.image} />
                    </View>

                    <View style={[styles.welcome_container]}>
                        <Text style={styles.welcome}>Eine Plattform für mündige Bürger, die</Text>
                    </View>
                    <View style={[styles.facts_container, {alignItems: 'flex-start',}]}>
                        <Text style={[styles.facts, { fontWeight: 'bold' }]}>Mit</Text>
                        <Text style={styles.facts}>denken</Text>
                    </View>
                    <View style={[styles.facts_container, {alignItems: 'center',}]}>
                        <Text style={[styles.facts, { fontWeight: 'bold' }]}>Mit</Text>
                        <Text style={styles.facts}>handeln</Text>
                    </View>
                    <View style={[styles.facts_container, {alignItems: 'flex-end',}]}>
                        <Text style={[styles.facts, { fontWeight: 'bold' }]}>Mit</Text>
                        <Text style={styles.facts}>leben</Text>
                    </View>

                    <Text style={styles.info}>
                        Du hast Ideen für deine Stadt, siehst Bedarf bei der städtischen Weiterenwicklung und willst, dass deine Meinung gehört wird.
                        </Text>
                </View>
            </StepLayout>
        )
    }
}


WelcomeScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    facts_container: {
        flexDirection: 'row'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.WelcomeScreen,
    },
    welcome: {
        fontSize: 15,
        textAlign: 'center',
        color: Colors.welcome,
    },
    welcome_container: {
        margin: 10,
        marginTop: 20
    },
    info: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: Colors.Text,
    },
    facts: {
        color: Colors.Text,
        marginBottom: 10,
        marginTop: 10,
        fontSize: 15,
    },
    image: {
        width: 100,
        height: 144
    },
});
