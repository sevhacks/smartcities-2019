import React, {Component} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import {get} from 'lodash';
import MapView, {Circle} from 'react-native-maps';
import MapSwiper from "../components/map/MapSwiper";
import Colors, {text} from "../constants/Colors";
import DefaultBlockButton from "../components/button/DefaultBlockButton";
import ColorBlockButton from "../components/button/ColorBlockButton";
import {Icon} from 'react-native-elements';

export default class ProjectsScreen extends Component {
    state = {
        activeItem: void 0,
        items: [{
            category: 0,
            id: 'sadf234',
            name: 'Skaterpark: Kronenwiese',
            coordinates: [],
            images: [
                'https://source.unsplash.com/1024x768/?nature',
                'https://source.unsplash.com/1024x768/?water',
                'https://source.unsplash.com/1024x768/?girl',
                'https://source.unsplash.com/1024x768/?tree'
            ],
            description: 'Offenburg hat auf der Agenda weitere Freizeitaktionen für Jugendliche zu gestalten. In diesem Zuge wird im Kronenareal eine 490 m² Fläche zur Verfügung. 3 Installationen werden gebaut, mit Nebenfläche zum Verweilen.',
            categories: [],
            createDate: new Date(),
            creator: {
                _id: '',
                name: '',
            }
        }, {
            category: 1,
            id: '12csfasf',
            name: 'Sitzflächen: Mühlbachareal',
            coordinates: [],
            images: [
                'https://source.unsplash.com/1024x768/?water',
                'https://source.unsplash.com/1024x768/?nature',
                'https://source.unsplash.com/1024x768/?girl',
                'https://source.unsplash.com/1024x768/?tree'
            ],
            description: 'Das neu erbaute Mühlbachareal wird durch Sitzflächen am Rande der Grünflächen ergänzt. Für Familien und Anwohner wird das Areal Anlaufstelle für Freizeitgestaltung und Erholung.',
            categories: [],
            createDate: new Date(),
            creator: {
                _id: '',
                name: '',
            }
        }],
        loading: false,
        initialRegion: {
            latitude: 37.78825,
            longitude: -122.4324,
        }
    };

    componentDidMount() {

    }

    onPressCategory = (event) => {
        const {navigation} = this.props;
        navigation.navigate('Category', {});
    };

    onPressAdd = (event) => {
        console.log(event);
    };

    onPressCityOrDistrict = (event) => {
        console.log(event);
    };

    onPressStatus = (event) => {
        console.log(event);
    };

    onChangeCarousel = (index) => {
        const {items} = this.state;
        this.setState({aciteItem: items[index]});
    };

    render() {
        const {navigation} = this.props;
        const {loading, items, acitveItem, initialRegion} = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.filter}>
                    <View style={{flexDirection: 'row', paddingTop: 40}}>
                        <View style={{padding: 8}}>
                            <ColorBlockButton title={'Stadt'}
                                              color={Colors.success}
                                              onPress={this.onPressCityOrDistrict}/>
                        </View>
                        <View style={{padding: 8}}>
                            <DefaultBlockButton title={'Kategorie'}
                                              onPress={this.onPressCategory}/>
                        </View>
                        <View style={{padding: 8}}>
                            <DefaultBlockButton title={'Status'}
                                                onPress={this.onPressStatus}/>
                        </View>
                        <View style={{flex: 1}}/>
                    </View>
                </View>
                <MapView initialRegion={{latitude: 48.4735, longitude: 7.9498,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421}} style={styles.mapStyle}/>
                <Icon raised
                      containerStyle={{position: 'absolute', right: 16, top: 130}}
                      reverse
                      name='add'
                      size={22}
                      color={Colors.success}
                      onPress={this.onPressAdd}/>
                <MapSwiper loading={loading}
                           navigation={navigation}
                           onChangeCarousel={this.onChangeCarousel}
                           data={items || []}/>
            </View>
        );
    }
}

ProjectsScreen.navigationOptions = {
    header: null,
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    mapStyle: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    filter: {
        width: '100%',
        height: 120,
        position: 'absolute',
        backgroundColor: Colors.body,
        borderBottomWidth: 1,
        borderColor: Colors.border,
        top: 0,
        right: 0,
        left: 0,
        zIndex: 1,
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
        padding: 8
    },
    scrollFilter: {
        height: 40
    }
});
