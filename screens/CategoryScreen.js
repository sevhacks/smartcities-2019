import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors, {text} from "../constants/Colors";
import ListItem from "../components/list/ListItem";

export default class CategoryScreen extends Component {
    state = {};

    onPressContinue = async () => {
        const {navigation} = this.props;
        navigation.navigate('Home', {});

    };

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    onPressItem = () => {

    };

    onPressSave = () => {

    };

    render() {
        const {} = this.props;
        const {} = this.state;
        return (
            <StepLayout title={'Kategorie'}
                        color={Colors.success}
                        onPressContinue={this.onPressBack}
                        continueText={'Speichern'}>
                <View style={{flex: 1}}>
                    <ListItem onPress={() => this.onPressItem()}
                              title={'Projekte'}
                              iconColor={Colors.success}
                              icon={'check-circle'}/>
                    <ListItem onPress={() => this.onPressItem()}
                              title={'Ideen'}
                              iconColor={Colors.border}
                              icon={'lens'}/>
                    <ListItem onPress={() => this.onPressItem()}
                              title={'Missstände'}
                              iconColor={Colors.border}
                              icon={'lens'}/>
                </View>
            </StepLayout>
        )
    }
}


CategoryScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    text: {
        color: Colors.success
    },
});
