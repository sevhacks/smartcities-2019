import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors, {text} from "../constants/Colors";
import {Icon} from 'react-native-elements';

export default class VertifyScreen extends Component {
    state = {};

    onPressContinue = async () => {
        const {navigation} = this.props;
        navigation.navigate('Home', {});

    };

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    onPressUpload = () => {

    };

    render() {
        const {} = this.props;
        const {firstname, lastname} = this.state;
        return (
            <StepLayout title={'Bestätigung der Person'}
                        color={Colors.danger}
                        onPressBack={this.onPressBack}
                        defaultContinueButton
                        continueText={'Später'}
                        onPressContinue={this.onPressContinue}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.help}>Bitte laden Sie Ihren Ausweis hoch, damit wir Sie bestätigen können.</Text>
                    <Icon raised
                          reverse
                          name='cloud-upload'
                          size={50}
                          color={Colors.danger}
                          onPress={this.onPressUpload}/>
                </View>
            </StepLayout>
        )
    }
}


VertifyScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    text: {
        color: Colors.danger
    },
    example: {
        flex: 1,
        backgroundColor: Colors.success
    },
    help: {
        textAlign: 'center',
        color: Colors.mutedText,
        paddingHorizontal: 32,
        paddingVertical: 16,
    }
});
