import React, {Component} from 'react';
import {StyleSheet, View, Text, Picker} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors from "../constants/Colors";
import TextInputField from "../components/form/TextInputField";
import Form from "../modules/FormValidation/Form";


export default class SalutationScreen extends Component {
    state = {
        firstname: '',
        lastname: '',
        salutation: '',
    };

    onPressContinue = async () => {
        const {navigation} = this.props;
        navigation.navigate('Signin', {});

    };

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    onChangeText = (value) => {
        this.setState({...value});
    };


    onSubmit = (value) => {
        this.refs.form.submit();
    };

    render() {
        const {} = this.props;
        const {firstname, lastname, salutation} = this.state;
        return (
            <StepLayout title={'Persönliche Daten'}
                        color={Colors.danger}
                        onPressBack={this.onPressBack}
                        onPressContinue={this.onPressContinue}>
                
                <Form style={{flex: 1}} onSubmit={this.onSubmit} ref='form'>
                    <Picker
                        selectedValue={salutation}
                        mode='dropdown'
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({salutation: itemValue})
                        }>
                        <Picker.Item label="Herr" value="Herr" />
                        <Picker.Item label="Frau" value="Frau" />
                        <Picker.Item label="Divers" value="Divers" />
                    </Picker>
                    <TextInputField name='firstname'
                                    label={'Vorname'}
                                    autoCapitalize='none'
                                    validators={['required']}
                                    errorMessages={[
                                        'Wird benötigt',
                                    ]}
                                    onChangeText={this.onChangeText}
                                    autoCorrect={false}
                                    placeholder='Vorname'
                                    value={firstname}/>
                    <TextInputField name='lastname'
                                    label={'Nachname'}
                                    autoCapitalize='none'
                                    validators={['required']}
                                    errorMessages={[
                                        'Wird benötigt',
                                    ]}
                                    onChangeText={this.onChangeText}
                                    autoCorrect={false}
                                    placeholder='Nachname'
                                    value={lastname}/>
                </Form>
            </StepLayout>
        )
    }
}


SalutationScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    text: {
        color: Colors.danger
    },
    example: {
        flex: 1,
        backgroundColor: Colors.success
    }
});
