import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors from "../constants/Colors";
import ListItem from "../components/list/ListItem";

export default class ProfileScreen extends Component {
    state = {
        email: '',
        password: ''
    };

    onPressContinue = () => {

    };

    onPressBack = () => {

    };

    onSubmit = () => {

    };

    onChangeText = (value) => {
        this.setState({...value});
    };


    render() {
        return (
            <StepLayout title={'Profile'}
                        color={Colors.danger}>
                <View style={styles.section}>
                    <ListItem iconColor={Colors.danger} icon={'account-circle'} title={'Persönliche Daten'} subtitle={'Sobchak Walter'}/>
                    <ListItem iconColor={Colors.danger} icon={'email'} title={'Anmeldedaten'} subtitle={'sobchak.walter@sevdesk.de'}/>
                    <ListItem iconColor={Colors.danger} icon={'location-city'} title={'Wohnort'} subtitle={'Offenburg'}/>
                    <ListItem iconColor={Colors.danger} icon={'check-circle'} title={'Verifiziert'} subtitle={'Ist vertifiziert'}/>
                </View>
            </StepLayout>
        )
    }
}


ProfileScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    section: {
        backgroundColor: '#fff',
        position: 'relative'
    },
    container: {
        flex: 1,
        padding: 12,
    },
    name: {
        fontSize: 28,
        fontWeight: 'bold',
    },
    values: {
        fontSize: 28,
    },
});