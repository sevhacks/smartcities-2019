import React, {Component} from 'react';
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import {StackedAreaChart, ProgressCircle, PieChart, XAxis} from 'react-native-svg-charts'
import StepLayout from '../components/layout/StepLayout'
import * as shape from 'd3-shape'
import Colors from "../constants/Colors";

export default class CommuneScreen extends Component {
    render() {
        const { } = this.props;
        const data = [
            {
                month: new Date(2017, 0, 1),
                citizen: 640,
            },
            {
                month: new Date(2018, 1, 1),
                citizen: 1600,
            },
            {
                month: new Date(2019, 2, 1),
                citizen: 4640,
            },
        ];
        const data2 = [
            {
                value: 2000,
                svg: {
                    fill: Colors.danger,
                    onPress: () => console.log('press', 0),
                },
                key: 1,
            },
            {
                value: 3000,
                svg: {
                    fill: Colors.primary,
                    onPress: () => console.log('press', 1),
                },
                key: 2,
            },
            {
                value: 1000,
                svg: {
                    fill: Colors.success,
                    onPress: () => console.log('press', 2),
                },
                key: 3,
            },
        ];
        const colors = [Colors.warning];
        const keys = ['citizen'];
        const svgs = [
            { onPress: () => console.log('citizen') },
        ];


     
        return (
            <StepLayout title={'Offenburg'}
                        color={Colors.warning}>
                <View style={styles.section}>
                    <Text style={styles.headliner}>Budget von der Gemeinde</Text>
                    <View style={styles.relativeView}>
                        <ProgressCircle style={{height: 200}} progress={0.7} progressColor={Colors.warning}/>
                        <View style={styles.absoluteOverlayer}>
                        <Text style={styles.values2}>32.000€</Text>
                            <Text style={styles.values}>25.000€</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.section}>
                    <Text style={styles.headliner}>Projekte der Gemeinde</Text>
                    <View style={styles.relativeView}>
                    <PieChart style={{height: 200}} 
                     valueAccessor={({ item }) => item.value}
                    data={data2}>
                </PieChart>
                <View style={styles.absoluteOverlayer2}>
                        <Text style={{color: Colors.danger}}>Abgelehnt</Text>
                            <Text  style={{color: Colors.primary}}>Genemigt</Text>
                            <Text  style={{color: Colors.success}}>Umgesetzt</Text>
                        </View>
                </View>
                    </View>
                    <View style={styles.section}>
                        <Text style={styles.headliner}>Teilnahme von Bürgern</Text>
                        <StackedAreaChart
                            style={{ height: 100, paddingVertical: 16 }}
                            data={data}
                            keys={keys}
                            colors={colors}
                            curve={shape.curveNatural}
                            showGrid={true}
                            svgs={svgs}
                        />
                        <XAxis
                            style={{ marginTop: 0, marginHorizontal: 10 }}
                            svg={{
                                marginTop: 0,
                                fill: Colors.warning,
                                fontWeight: 'bold',
                                fontSize: 12,
                            }}
                            data={data}
                            formatLabel={(value,index) => index === 0 ? '2017' : index === 1 ? '2018' : '2019'}
                            contentInset={{ left: 15, right: 15, top: 0}}
                            labelStyle={{ color: Colors.warning }}
                        />
                    </View>
            </StepLayout>
        )
    }
}


CommuneScreen.navigationOptions = {
    header: null,
};


const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.warning,
    },
    header: {
        backgroundColor: Colors.warning,
        height: 140,
        paddingHorizontal: 16,
        paddingTop: 32,
    },
    body: {
        backgroundColor: '#fff',
        marginTop: -16,
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        paddingHorizontal: 16,
    },
    section: {
        backgroundColor: '#fff',
        paddingVertical: 16,
        position: 'relative'
    },
    relativeView: {
        position: 'relative'
    },
    absoluteOverlayer: {
        backgroundColor: 'transparent',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0
    },
    absoluteOverlayer2: {
        backgroundColor: 'transparent',
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0
    },
    name: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#fff'
    },
    values: {
        fontSize: 28,
        fontWeight: 'bold',
        color: Colors.warning
    },
    values2: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#DADADA'
    },
    citizen: {
        paddingTop: 8,
        fontSize: 12,
        fontWeight: 'bold',
        color: 'rgba(255, 255, 255, 0.7)'
    },
    headliner: {
        paddingVertical: 8,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333'
    },
});
