import React, {Component} from 'react';
import {StyleSheet, View, Text, ScrollView, Dimensions} from 'react-native';
import {StackedAreaChart, ProgressCircle, PieChart} from 'react-native-svg-charts';
import StepLayout from '../components/layout/StepLayout'
import * as shape from 'd3-shape';
import Colors from "../constants/Colors";
import Carousel from 'react-native-snap-carousel';
import ImageSlider from 'react-native-image-slider';
import LinkButton from "../components/button/LinkButton";


export class HomeScreenBox extends Component {

    render() {
        const {item} = this.props;
        return (
            <View style={styles.homebox}>
                {item.image && <ImageSlider images={[item.image]}
                                            style={styles.imageSlider}/>}
                <View style={styles.homeboxBody}>
                    <Text numberOfLines={item.image ? 1 : 2} style={styles.subtitle}>{item.subtitle}</Text>
                    {!item.image && <Text numberOfLines={6} style={styles.text}>{item.text}</Text>}
                </View>
            </View>
        );
    }
}

export default class HomeScreen extends Component {
    render() {
        const {} = this.props;
        const iconColor = null;
        const data = [
            {
                month: new Date(2015, 0, 1),
                citizen: 640,
            },
            {
                month: new Date(2015, 1, 1),
                citizen: 1600,
            },
            {
                month: new Date(2015, 2, 1),
                citizen: 4640,
            },
        ];
        const rowOne = [{
            title: 'Fakten über Offenburg',
            subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
            text: null,
            facts: [
                {text: "Größte Stadt des Ortenaukreises (420.000 Einwohner)"},
                {text: "60.000 Einwohnern"},
                {text: "Davon 50.000 Erwerbstätige"},
            ],
        },
        {
            title: 'Politik',
            text: null,
            facts: [
                {text: "Oberbürgermeister Marco Steffens (CDU)"},
                {text: "Gemeinderat: Grüne 11 Sitze CDU 10 Sitze, FWO 6 Sitze, SPD 6 Sitze, FDP 3 Sitze, AfD 3 Sitze, OL 1 Sitze"},
                {text: "Stadt Offenburg Historisches Rathaus"}
            ],
        },
        {
            title: 'Lokale Agenda 21',
            text: null,
            facts: [
                {text: "Das Leitbild will die gesamte Bürgerschaft, Frauen und Männer gleichermaßen, ansprechen und ermutigen, bei der Gestaltung der Zukunft ihrer Stadt mitzuwirken mehr"}
            ],
        }
        ];
        const news = [{
            title: null,
            subtitle: 'Badische Zeitung: "Smart Cities" / Dritter Blackforest Hackathon im Technologiepark Offenburg (TPO) / Zweitägiger Entwicklungsmarathon.',
            text: 'OFFENBURG (BZ). "Smart Cities" heißt das Motto des dritten Blackforest Hackathons, der vom 25. bis zum 27.Oktober im Technologiepark Offenburg (TPO) stattfindet. Rund einhundert Nachwuchstalente aus Informatik, Design und Wirtschaft tüfteln im Rahmen des zweitägigen … mehr',
            facts: [],
        }, {
            title: null,
            subtitle: 'Stuttgarter Zeitung: Start-up in Offenburg Milliardenchance in der Provinz',
            text: 'Start-ups, die abseits der Metropolen entstehen, sind in Baden-Württemberg zu wenig im Blick - sagt Fabian Silberer, Gründer der erfolgreichen IT-Firma Sevdesk aus Offenburg. Stuttgart - Wo entstehen Gründerideen? In einem Berliner Hipster-Café? Auf einer Netzwerk-Party im Silicon Valley? Auf dem Innovationscampus eines IT-Unternehmens? Oder wie wäre es mit  … mehr',
            facts: [],
        },
        {
            title: null,
            subtitle: 'Fallstaff: Das sind die beliebtesten Pizzerien Deutschlands 2019',
            text: 'Die Sieger im Überblick: Baden: Pizzeria la Famiglia – Schaible Stadion, Offenburg Dass Pizza auch in einer ehemaligen Sportgaststätte schmecken kann, beweist in Offenburg Familie Morreale! mehr',
            facts: [],
        }
        ];
        const events = [{
                title: null,
                subtitle: 'Sonntag, 27. Oktober 2019, 09:30',
                text: 'Familien‑Kunstsonntag: Ton: kleine Porträtbüsten Kunstschule Offenburg Weingartenstrasse 34B, Offenburg Quelle: ortenaukultur.de',
                facts: [],
            }, {
                title: null,
                subtitle: 'Sonntag, 27. Oktober 2019, 19:00',
                text: 'Aakash Odedra Company Oberrheinhalle Schutterwälder Straße 3, Offenburg Quelle: eventful.com',
            },
            {
                title: null,
                subtitle: 'Donnerstag, 31. Oktober 2019, 21:00',
                text: 'Halloween Party Nightclub Roxy Bar Hauptstraße 100, Offenburg Quelle: surstrugmming.ga',
            },
            {
                title: null,
                subtitle: 'Samstag, 2. NOVEMBER 2019',
                text: 'DIE 2000ER PARTY FREIRAUM OFFENBURG MARLENERSTRASSE 5, OFFENBURG Quelle: https://www.freiraum-offenburg.de',
            }
        ];

        const projects = [
            {
                title: null,
                subtitle: 'Stadterneuerung / Sanierung',
                text: 'Was heißt Stadterneuerung: Ziele, Rechtsgrundlagen der Sanierung Ziel der Stadterneuerung ist die Verbesserung der Wohn-, Lebens- und Arbeitsplatzqualität sowie der Infrastruktur eines Gebiets. Dieses Ziel wird einerseits durch private Maßnahmen und anderseits durch kommunale Maßnahmen erreicht. mehr',
                facts: [],
            },
            {
                title: null,
                subtitle: 'Städtische Bauplätze',
                text: 'Der Verkauf von städtischen Bauplätzen ist eine freiwillige Leistung der Stadt Offenburg. Sie erfolgt unter Abwägung der Interessen der Gesamtstadt. Grundsätzlich werden zur Veräußerung anstehende städtische Bauplätze öffentlich ausgeschrieben. Der Verkauf von Bauplätzen für den Wohnungsbau erfolgt nach den städtischen Vergaberichtlinien. Mehr / AKTUELLE ANGEBOTE',
                facts: [],
            },
            {
                title: null,
                subtitle: 'Nördliche Innenstadt Ein neues Quartier zum Einkaufen, Wohnen und Arbeiten',
                text: 'Zwischen Gustav-Rée-Anlage, nördlicher Hauptstraße und der Unionbrücke wird von 2017 bis 2020 ein neues Innenstadtquartier zum Einkaufen, Wohnen und Arbeiten mit ansprechender Architektur und hoher städtebaulicher Qualität entstehen. Das Rée Carré entsteht mehr...',
                facts: [],
            },
            {
                title: null,
                subtitle: 'Verkehr - B33-Autobahnzubringer Offenburg-Süd',
                text: 'Dieses Projekt wird vom Regierungspräsidium Freiburg, Abteilung Straßenwesen und Verkehr, geplant. Aktuelle Informationen finden Sie auf der Homepage des Regierungspräsidiums Freiburg unter https://rp.baden-wuerttemberg.de/rpf/Abt4/Ref44/B33-Autobahnzubringer-Offenburg/Seiten/default.aspx',
                facts: [],
            }
        ];
        const loading = false;
        const colors = ['rgb(134, 65, 244)'];
        const keys = ['citizen'];
        const svgs = [
            {onPress: () => console.log('citizen')},
        ];
        return (
            <StepLayout title={'Startseite'}
                        color={Colors.primary}>

                <View style={styles.container}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 8}}>
                        <Text style={styles.headliner}>News</Text>
                        <LinkButton title={'Mehr erfahren'}
                                    color={Colors.primary}/>
                    </View>
                    <Carousel
                        ref={(d) => {
                            this._carousel = d;
                        }}
                        data={news}
                        renderItem={({item, index}) => (<HomeScreenBox item={item}/>
                        )}
                        itemWidth={Dimensions.get('window').width / 2}
                        sliderWidth={Dimensions.get('window').width}
                    />





                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 16}}>
                        <Text style={styles.headliner}>Events</Text>
                        <LinkButton title={'Mehr erfahren'}
                                    color={Colors.primary}/>
                    </View>
                    <Carousel
                        ref={(d) => {
                            this._carousel1 = d;
                        }}
                        data={events}
                        renderItem={({item, index}) => (<HomeScreenBox item={item}/>
                        )}
                        itemWidth={Dimensions.get('window').width / 2}
                        sliderWidth={Dimensions.get('window').width}
                    />



                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 16}}>
                        <Text style={styles.headliner}>Projekte</Text>
                        <LinkButton title={'Mehr erfahren'}
                                    color={Colors.primary}/>
                    </View>
                    <Carousel
                        ref={(d) => {
                            this._carousel2 = d;
                        }}
                        data={projects}
                        renderItem={({item, index}) => (<HomeScreenBox item={item}/>
                        )}
                        itemWidth={Dimensions.get('window').width / 2}
                        sliderWidth={Dimensions.get('window').width}
                    />
                </View>
            </StepLayout>
        )
    }
}

HomeScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#5600dd',
    },
    header: {
        backgroundColor: '#5600dd',
        height: 200,
        paddingHorizontal: 16,
        paddingTop: 32,
    },
    body: {
        backgroundColor: '#fff',
        marginTop: -16,
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        paddingHorizontal: 16,
    },
    section: {
        backgroundColor: '#fff',
        paddingVertical: 16,
    },
    name: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#fff'
    },
    citizen: {
        paddingTop: 8,
        fontSize: 12,
        fontWeight: 'bold',
        color: 'rgba(255, 255, 255, 0.7)'
    },
    headliner: {
        paddingVertical: 8,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333'
    },
    homebox: {
        width: Dimensions.get('window').width / 2,
        borderStyle: "solid",
        borderWidth: 1,
        height: 200,
        backgroundColor: '#fff',
        borderColor: Colors.border,
        borderRadius: 16,
    },
    homeboxBody: {
        padding: 16,
    },
    boxtitle: {
        fontSize: 20,
        textAlign: 'center',
        color: '#fff',
    },
    boxlistitem: {
        fontSize: 12,
        color: '#fff',
    },
    boxtext: {
        marginTop: 2,
        marginBottom: 2,
    },
    subtitle: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingBottom: 8
    },
    text: {
        fontSize: 16
    }

});
