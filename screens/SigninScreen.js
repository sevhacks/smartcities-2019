import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors from "../constants/Colors";
import TextInputField from "../components/form/TextInputField";
import Form from "../modules/FormValidation/Form";

export default class SigninScreen extends Component {
    state = {
        password: '',
        passwordRepeat: '',
        email: ''
    };

    onPressContinue = async () => {
        const {navigation} = this.props;
        navigation.navigate('Vertify', {});

    };

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    onChangeText = (value) => {
        this.setState({...value});
    };

    render() {
        const {} = this.props;
        const {password, passwordRepeat, email} = this.state;
        return (
            <StepLayout title={'Anmeldung'}
                        color={Colors.danger}
                        onPressBack={this.onPressBack}
                        onPressContinue={this.onPressContinue}>
                <Form style={{flex: 1}} onSubmit={this.onPressContinue}>
                    <TextInputField name='email'
                                    label={'E-Mail'}
                                    autoCapitalize='none'
                                    keyboardType='email-address'
                                    validators={['required', 'isEmail']}
                                    errorMessages={[
                                        'Wird benötigt',
                                        'Keine valide E-Mail',
                                    ]}
                                    onChangeText={this.onChangeText}
                                    autoCorrect={false}
                                    placeholder='E-Mail'
                                    value={email}/>
                    <TextInputField name='password'
                                    label={'Password'}
                                    autoCapitalize='none'
                                    secureTextEntry
                                    validators={['required']}
                                    errorMessages={[
                                        'Wird benötigt',
                                    ]}
                                    onChangeText={this.onChangeText}
                                    autoCorrect={false}
                                    placeholder='Password'
                                    value={password}/>
                    <TextInputField name='passwordRepeat'
                                    label={'Password wiederholen'}
                                    autoCapitalize='none'
                                    secureTextEntry
                                    validators={['required']}
                                    errorMessages={[
                                        'Wird benötigt',
                                    ]}
                                    onChangeText={this.onChangeText}
                                    autoCorrect={false}
                                    placeholder='Password wiederholen'
                                    value={passwordRepeat}/>
                </Form>
            </StepLayout>
        )
    }
}


SigninScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    text: {
        color: Colors.danger
    },
    example: {
        flex: 1,
        backgroundColor: Colors.success
    }
});
