import React, {Component} from 'react';
import {StyleSheet, View, Text, ScrollView, TouchableOpacity} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors from "../constants/Colors";

export default class JoinScreen extends Component {
    state = {
        tab: 0
    };

    onPressContinue = async () => {
        const {navigation} = this.props;
        navigation.navigate('Location', {});

    };

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    render() {
        const {} = this.props;
        const {tab} = this.state;
        return (
            <StepLayout title={'Mitmachen'}
                        color={Colors.success}
                        backIcon={'close'}
                        onPressBack={this.onPressBack}
                        onPressContinue={this.onPressContinue}
                        continueText={'Speichern'}>
                <View style={styles.container}>
                    <ScrollView horizontal>
                        <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity onPress={() => this.onPressTab(0)} style={[styles.tabbar]}>
                                <Text style={[styles.tabbarText, tab === 0 && {color: Colors.success}]}>Pro</Text>
                            </TouchableOpacity>
                            <View style={{width: 1, backgroundColor: Colors.border}}/>
                            <TouchableOpacity onPress={() => this.onPressTab(1)} style={[styles.tabbar]}>
                                <Text style={[styles.tabbarText, tab === 1 && {color: Colors.success}]}>Contra</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1}}>

                        </View>
                    </ScrollView>
                </View>
            </StepLayout>
        )
    }
}


JoinScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    tabbar: {
        flex: 1,
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabbarText: {
        fontWeight: "bold",
        fontSize: 16
    }
});
