import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import StepLayout from "../components/layout/StepLayout";
import Colors from "../constants/Colors";
import Loader from "../components/loader/Loader";
import SearchField from "../components/form/SearchField";
import ListItem from '../components/list/ListItem'
import GoogleHttpService from '../services/GoogleHttpService'

export default class LocationScreen extends Component {
    state = {
        searchText: 'Offen',
        list: [
            {
                "_id": "ChIJCduT2xHTlkcRsIXfpbdrHwQ",
                "placeId": "ChIJCduT2xHTlkcRsIXfpbdrHwQ",
                "location": "Offenburg, Deutschland"
            }, {
                "_id": "ChIJ_UdLNRsSvUcREMawKVBDIgQ",
                "placeId": "ChIJ_UdLNRsSvUcREMawKVBDIgQ",
                "location": "Offenbach am Main, Deutschland"
            }, {
                "_id": "EjdPZmZlbmJhY2hlciBMYW5kc3RyYcOfZSwgRnJhbmtmdXJ0IGFtIE1haW4sIERldXRzY2hsYW5kIi4qLAoUChIJ06pnz9QNvUcRb9H_V4_w3u4SFAoSCR-rhwNxC71HEcaQriIqL0c-",
                "placeId": "EjdPZmZlbmJhY2hlciBMYW5kc3RyYcOfZSwgRnJhbmtmdXJ0IGFtIE1haW4sIERldXRzY2hsYW5kIi4qLAoUChIJ06pnz9QNvUcRb9H_V4_w3u4SFAoSCR-rhwNxC71HEcaQriIqL0c-",
                "location": "Offenbacher Landstraße, Frankfurt am Main, Deutschland"
            }, {
                "_id": "ChIJx3g4mHhtvUcRQcSOl8IUUZw",
                "placeId": "ChIJx3g4mHhtvUcRQcSOl8IUUZw",
                "location": "Offenthal, Dreieich, Deutschland"
            }, {
                "_id": "ChIJi_5iIoJacUcR5P3YCoj2NTs",
                "placeId": "ChIJi_5iIoJacUcR5P3YCoj2NTs",
                "location": "Offensee, Ebensee, Österreich"
            }
        ],
        loading: false,
        error: void 0
    };

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    onPressItem = (item) => {
        const {navigation} = this.props;
        this.setState({loading: false});
        navigation.navigate('Salutation', {
            location: {
                ...item,
                viewport: {
                    northeast: {
                        lat: 48.5526926,
                        lng: 8.0284841
                    },
                    southwest: {
                        lat: 48.3968244,
                        lng: 7.861641
                    }
                }
            }
        });
    };

    onPressSearchTextClear = () => {
        this.setState({searchText: '', list: [], loading: false});
    };

    onTextChange = (searchText) => {
        this.setState({searchText, loading: true, error: void 0});
        GoogleHttpService.getAddressesByName(searchText)
            .then(response => {
                this.setState({loading: false, list: response});
            })
            .catch((error, data) => {
                this.setState({error: !!error, loading: false});
            });
    };


    render() {
        const {} = this.props;
        const {loading, list, searchText} = this.state;
        return (
            <StepLayout title={'Wohnort'}
                        bodyContainer={styles.bodyContainer}
                        color={Colors.danger}
                        onPressBack={this.onPressBack}>
                <SearchField value={searchText}
                             style={{paddingTop: 16}}
                             placeholder='Suche nach deiner Stadt / Gemeinde'
                             onPressClear={this.onPressSearchTextClear}
                             onChange={this.onTextChange}/>
                <View style={styles.list}>
                    {list.map((item, index) => <ListItem onPress={() => this.onPressItem(item)}
                                                         key={item.id + '' + index}
                                                         icon={item.history ? 'history' : item.icon}
                                                         iconColor={item.history ? Colors.primary : Colors.primary}
                                                         divider={(index + 1) !== list.length}
                                                         title={item.location}/>)}
                    {loading ? <View style={styles.loader}><Loader/></View> : null}
                </View>
            </StepLayout>
        );
    }
}


LocationScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    bodyContainer: {
        paddingHorizontal: 0
    },
    list: {
        flex: 1
    }
});
